export BLACKBOARD_DEBUG=false
export TITLE_COLORS=magenta gras
#export TITLE_COLORS=green gras
export TITLE_COLORS=black gras
export TITLE_COLORS=orange
export LINE_LEN="50"
export TEXT_TAB="1"
export TEXT_COLORS=orange
#export TEXT_COLORS=magenta gras
#export TEXT_COLORS=green gras
export TEXTBLOCK_COLORS=orange
#export TEXTBLOCK_COLORS=black

export COMMENT_COLORS=magenta
#export COMMENT_COLORS=black
export ERROR_COLORS=red

#export OBJECT_COLORS=magenta gras
export OBJECT_COLORS=blue gras
export current_colors=""

#==============================================================   

function repeatChar() {

#==============================================================   

    local string="$1"
    local count="$2"
    for (( k=0 ; a=1+2*k , k<$count ;  k++)); do
        printf '%s' "$string"
    done

}
#==============================================================   

function DEBUG() {

#==============================================================   
    export BLACKBOARD_DEBUG=$1
    #MESSAGE "debug mode"

}
#==============================================================   

function LIST() {

#==============================================================   

    MESSAGE "    * $@"
}
#==============================================================   

function set_colors() {

#==============================================================   

    export current_colors=$@
}
#==============================================================   

function reset_colors() {

#==============================================================   

    export current_colors=""
    text_reset
}
#==============================================================   

function MESSAGE() {

#==============================================================   

    if( $BLACKBOARD_DEBUG == true )
    then
        local str="# $(repeatChar ' ' $TEXT_TAB) $@"
        if [ "$current_colors" == "" ]
        then
             echo -e "$(text $TEXT_COLORS)$str$(text_reset)"
        else
            echo -e "$(text $current_colors)$str"
        fi
    fi
}
#==============================================================   

function TITLE() {

#==============================================================     
    #terminator_title "$@"

    MESSAGE
    set_colors "$TITLE_COLORS"
    MESSAGE "#####################################################"
    MESSAGE "                   $@"
    MESSAGE "#####################################################"
    reset_colors


}
#==============================================================   

function SUBTITLE() {

#==============================================================     
    #terminator_title "$@"
    MESSAGE
    set_colors "$TITLE_COLORS"
    MESSAGE "===================================================="
    MESSAGE "              $@"
    MESSAGE "===================================================="
    reset_colors


}

#=========================================================== 

function PARAGRAPH() {

#==============================================================     
    #terminator_title "$@"
    MESSAGE
    set_colors "$TITLE_COLORS"
    MESSAGE "              $@"
    MESSAGE "-----------------------------------------------------"
    reset_colors

}
#=========================================================== 

function PARAGRAPH2() {

#==============================================================     

    MESSAGE
    set_colors "$TITLE_COLORS"
    MESSAGE "$@"
    reset_colors

}
#=========================================================== 

function SEPARATOR() {

#==============================================================     

    MESSAGE
    set_colors "$TITLE_COLORS"
    MESSAGE "#####################################################"
    reset_colors
    

}
#=========================================================== 

function ERROR() {

#==============================================================     

    
    set_colors "$ERROR_COLORS"
    MESSAGE "ERROR : $@$(reset_colors)"

    

}
######################################################

function TEXTBLOCK() {

######################################################

    local IFS=$'\n'
    #local IFS=
    
    for elt in $(cat)
    do
        if [[ $elt = *http* ]]
        then
            set_colors "$OBJECT_COLORS"
        elif [[ $elt = ======* ]]
        then
            set_colors "$COMMENT_COLORS"

        elif [[ $elt = -------* ]]
        then
            set_colors "$COMMENT_COLORS"
        elif [[ $elt = "   "* ]]
        then
            set_colors "blue"
        elif [[ $elt = \#* ]]
        then
            set_colors "$COMMENT_COLORS"
        else
            set_colors "$TEXTBLOCK_COLORS"
        fi

        MESSAGE "       $elt"

        if [[ $elt = ======* ]]
        then
            MESSAGE

        elif [[ $elt = -------* ]]
        then
            MESSAGE
        fi
    done
    reset_colors
}
#=========================================================== 
