

function SYNC_SOURCE() {
    GLOBAL "SYNC_SOURCE" "$1"
}

function SYNC_TARGET() {
    GLOBAL "SYNC_TARGET" "$1"
}

function SYNC_LOCAL() {
    DIR $SYNC_TARGET/$1
    RSYNC_LOCAL_MIRROR "$SYNC_SOURCE/$1" "$SYNC_TARGET/$1"
}



function SYNC_DIRS() {


    for media in $(ls_dir $SYNC_SOURCE/$1)
    do
        name=$(basename $media)
        SYNC_LOCAL "$1/$name"
    done

}

function SYNC_NT() {

    RSYNC_NT_MIRROR "$SYNC_SOURCE/$1" "$SYNC_TARGET/$1"
}
